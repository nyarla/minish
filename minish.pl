#!/usr/bin/perl

use strict;
use warnings;

package minish;

our $VERSION = '0.01';

use FindBin ();
use CGI::Carp qw(fatalsToBrowser);

# -- accessor ------------------------ #

our %stash = ();

for my $name ( qw( config plugins methods variables pages ) ) {
    no strict 'refs';
    *{$name} = sub {
        my $app = shift;
        if ( @_ ) {
            $stash{$name} = shift;
        }
        else {
            return $stash{$name};
        }
    }
}

for my $method ( qw( template interpolate ) ) {
    no strict 'refs';
    *{$method} = sub {
        my ( $app, @args ) = @_;
        $app->methods->{$method}->( $app, @args );
    };
}

for my $name ( qw( cgi url path_info path_arguments flavour header output ) ) {
    no strict 'refs';
    *{$name} = sub {
        my $self = shift;
        if ( @_ ) {
            $self->{$name} = shift;
        }
        else {
            return $self->{$name}
        }
    }
}

__PACKAGE__->config({

    global  => {
        plugins_dir     => ["${FindBin::Bin}/plugins"],
        plugin_state_dir => "${FindBin::Bin}/state",
    },

    flavour => {
        dir => "${FindBin::Bin}/flavours",
    },

    pages   => {
        dir => "${FindBin::Bin}/pages",
        file_extension => 'txt',
    }

});

__PACKAGE__->methods({
    template    => sub {
        my ( $app, $path, $chunk, $flavour ) = @_;

        my $dir    = $app->config->{'flavour'}->{'dir'};
           $path ||= q{};

        do {
            if ( open( my $fh, '<', "${dir}/${path}/${chunk}.${flavour}" ) ) {
                my $data = do { local $/; <$fh> };
                close($fh);
                return $data;
            }
        }
        while ( $path =~ s{/*([^/]*)$}{} && $1 );

        return q{};
    },
    interpolate => sub {
        my ( $app, $template, $vars ) = @_;

        $vars ||= {};
        my $global = $app->variables;

        $template =~ s{<%([^\cx]+?)%>}{
            my $text = eval($1) || q{};
               $text = "Interpolate error: $@" if ( $@ );
               $text = $app->escape_html($text);
               $text;
        }gse;

        $template =~ s{<\$([^\cx]+?)\$>}{
            my $text = eval($1) || q{};
               $text = "Interpolate error: $@" if ( $@ );
               $text;
        }gse;

        return $template;
    },
});

__PACKAGE__->variables({});

# -- run ----------------------------- #

sub setup {
    my ( $class ) = @_;

    $class->setup_config;
    $class->setup_variable;
    $class->setup_plugins;
    $class->setup_methods;
    $class->setup_pages;

    return 1;
}

sub prepare {
    my ( $self ) = @_;

    $self->prepare_url;
    $self->prepare_path;
    $self->prepare_plugins;

}

sub run {
    my ( $self ) = @_;

    $self->header({});
    $self->output(q{});

    $self->run_plugins('prepare');

    my $path            = join q{/}, @{ $self->path_arguments };
    my $flavour         = $self->flavour;

    my $output          = q{};

    my $page            = $self->pages->page($path);

    my $content_type = $self->template( $path, 'content_type', $flavour );
       $content_type =~ s{\n.*}{}g;

    $self->header->{'-type'}    = $content_type;
    $self->header->{'-status'}  = '200 OK';

    if ( ! $self->run_plugin_first('skip') ) {
        my $head = $self->template( $path, 'head', $flavour );
        $self->run_plugins( head => \$head );
        $output .= $self->interpolate( $head, { page => $page } );
        $self->output( $output );

        my $content = $self->template( $path, 'page', $flavour );
        $self->run_plugins( page => $page, \$content );
        $output .= $self->interpolate( $content, { page => $page } );
        $self->output( $output );

        my $foot = $self->template( $path, 'foot', $flavour );
        $self->run_plugins( foot => \$foot );
        $output .= $self->interpolate( $foot, { page => $page } );
        $self->output( $output );
    }

    $self->run_plugins('finalize');

    print $self->cgi->header(%{ $self->header });
    print $self->output;
}

# -- setup --------------------------- #

sub setup_config {
    my ( $class ) = @_;

    my $file = $class->env_value('config');
    my $conf = eval { require $file };

    if ( $@ ) {
        die "Failed to load configuration file: $file: $@";
    }

    if ( ref $conf ne 'HASH' ) {
        die "Configutaion value is not HASH reference: $conf";
    }

    $class->merge_config( $conf );

}

sub setup_variable {
    my ( $class ) = @_;

    # global.plugins_dir
    {
        my $dirs = $class->config->{'global'}->{'plugins_dir'} || [];
           $dirs = [ $dirs ] if ( ref $dirs ne 'ARRAY' );
        $_ =~ s{/*$}{}g for @{ $dirs };
        $class->config->{'global'}->{'plugins_dir'} = $dirs;
    }

    # global.plugin_state_dir
    {
        $class->config->{'global'}->{'plugin_state_dir'} =~ s{/*$}{}g;
    }

    # flavour.dir
    {
        $class->config->{'flavour'}->{'dir'} =~ s{/*$}{}g;
    }

    # pages.dir
    {
        $class->config->{'pages'}->{'dir'} =~ s{/*$}{}g;
    }

    # variable
    {
        $class->variables(
            __merge_hash(
                $class->variables,
                ( $class->config->{'variables'} || {} )
            )
        );
    }

}

sub setup_plugins {
    my ( $class ) = @_;

    my $dirs = $class->config->{'global'}->{'plugins_dir'};
       $dirs = [ $dirs ] if ( ref $dirs ne 'ARRAY' );

    my $order = $class->config->{'plugins'};
       $order = [ $order ] if ( ref $order ne 'ARRAY' );

    $class->plugins([]);

    PLUGIN: for my $stash ( @{ $order } ) {
        my $module = $stash->{'plugin'};
        my $config = $stash->{'config'};
        my $package = $module;
           $package =~ s{^minish::plugin::}{};
           $package = "minish::plugin::${package}";

        for my $dir ( @{ $dirs } ) {
            my $path = "${dir}/${module}";

            if ( -e $path && -r _  ) {
                eval { require $path };
                die "Failed to load plugin: $module: $path: $@" if ( $@ );
                
                die "${package}->new is not defined: $module: $path"
                    if ( ! $package->can('new') );
                die "${package}->setup is not defined: $module: $path"
                    if ( ! $package->can('setup') );
                die "${package}->start is not defined: $module: $path"
                    if ( ! $package->can('start') );

                my $instance = $package->new( config => $config );
                my $on_off   = ( $instance->setup( $class ) ) ? 1 : -1 ;

                push @{ $class->plugins }, +{
                    plugin  => $instance,
                    on_off  => $on_off,
                };
                next PLUGIN;
            }
        }
        die "plugin ${module} is not found.";
    }
}

sub setup_methods {
    my ( $class ) = @_;

    for my $method ( keys %{ $class->methods } ) {
        if ( ref( my $sub = $class->run_plugin_first( $method ) ) eq 'CODE' ) {
            $class->methods->{$method} = $sub;
        }
    }

}

sub setup_pages {
    my ( $class ) = @_;

    my ( $dir, $file_extension, $meta_prefix )
        = @{ $class->config->{'pages'} }{qw( dir file_extension meta_prefix )};

    my $pages = minish::pages->new(
        dir             => $dir,
        file_extension  => $file_extension,
        meta_prefix     => $meta_prefix,
    );


    $class->pages( $pages );
}

# -- prepare ------------------------- #

sub prepare_url {
    my ( $self ) = @_;

    my $url   = $self->config->{'global'}->{'url'};
       $url ||= $self->cgi->url;

       $url   =~ s{^include:}{http:} if ( ( $ENV{'SERVER_PROTOCOL'} || q{} ) eq 'INCLUDED' );
       $url   =~ s{\Q$ENV{'PATH_INFO'}\E}{} if ( defined $ENV{'PATH_INFO'} );

       $url   =~ s{/*$}{}g;

    $self->url( $url );
}

sub prepare_path {
    my ( $self ) = @_;

    my $path_info   = q{};
    my @arguments   = ();
    my $flavour     = q{};

    my $source      = $self->cgi->path_info || $self->cgi->param('path') || q{/};
       $source     .= "index" if ( $source =~ m{/$} );
    my @path_info   = split m{/+}, $source;
    shift @path_info;

    while ( $path_info[0] && $path_info[0] !~ m{^(.+)\.(.+?)$} ) {
        my $fragument = shift @path_info;
        $path_info .= '/' . $fragument;
        push @arguments, $fragument;
    }

    if ( $path_info[$#path_info] && $path_info[$#path_info] =~ m{^(.+)\.(.+?)$} ) {
        my $path    = $1;
           $flavour = $2;

        $path_info .= "/$path.$flavour";
        push @arguments, $path;
    }
    else {
        $flavour = $self->cgi->param('flavour') || $self->config->{'flavour'}->{'default'} || q{html};
        $path_info .= ".$flavour";
    }

    $path_info =~ s{^/*|/*$}{}g;

    $self->path_info( $path_info );
    $self->flavour( $flavour );
    $self->path_arguments( \@arguments );
}

sub prepare_plugins {
    my ( $self ) = @_;

    for my $stash ( @{ $self->plugins } ) {
        my $plugin = $stash->{'plugin'};
        my $on_off = $stash->{'on_off'};

        if ( $on_off > 0 ) {
            $on_off = ( $plugin->start( $self ) ) ? 1 : -1;
        }

        $stash->{'on_off'} = $on_off;
    }

}

# -- util ---------------------------- #

sub new { bless {}, shift }

sub merge_config {
    my ( $class, $hash ) = @_;

    die "Argument is not HASH reference. Usage: minish->merge_config( \%config )"
        if ( ref $hash ne 'HASH' );

    $class->config( __merge_hash( $class->config, $hash ) );
}

sub plugin_state {
    my ( $app, $path ) = @_;

    my $base   = $app->config->{'global'}->{'plugin_state_dir'};
       $path ||= q{};

    my $full = "${base}/${path}";
       $full =~ s{/+}{/}g;

    return $full;
}

sub run_plugins {
    my ( $app, $method, @args ) = @_;

    my @ret;
    for my $stash ( @{ $app->plugins } ) {
        my $plugin  = $stash->{'plugin'};
        my $on_off  = $stash->{'on_off'};
        if ( $on_off > 0 && $plugin->can($method) ) {
            push @ret, $plugin->$method( $app, @args );
        }
    }
    return @ret;
}

sub run_plugin_first {
    my ( $app, $method, @args ) = @_;

    for my $stash ( @{ $app->plugins } ) {
        my $plugin  = $stash->{'plugin'};
        my $on_off  = $stash->{'on_off'};

        if (
            $on_off > 0
            && $plugin->can($method)
            && defined( my $ret = $plugin->$method( $app, @args ) )
        ) {
            return $ret;
        }
    }

    return;
}

sub env_value {
    my ( $app, $key ) = @_;

    my $prefix = uc( ref $app || $app );
       $key    = uc $key;
    my $env    = "${prefix}_${key}";

    if ( exists $ENV{$env} ) {
        return $ENV{$env};
    }

    return;
}

our %escape = (
    '&' => '&amp;',
    '<' => '&lt;',
    '>' => '&gt;',
    '"' => '&quot;',
    "'" => '&apos;',
);

sub escape_html     {
    my ( $app, $text ) = @_;

    my $escape_re = join q{|}, keys %escape;
    $text =~ s{($escape_re)}{$escape{$1}}g;

    return $text;
}

sub unescape_html   {
    my ( $app, $text ) = @_;

    my %table = ( map { $escape{$_} => $_ } keys %escape );
    my $escape_re = join q{|}, keys %table;

    $text =~ s{($escape_re)}{$table{$1}}g;

    return $text;
}

sub __merge_hash {
    my ( $left, $right ) = @_;

    if ( ! defined $right ) {
        return $left;
    }

    if ( ! defined $left ) {
        return $right;
    }

    die "Left hash is not HASH reference."
        if ( ref $left ne 'HASH' );

    die "Right hash is not HASH reference."
        if ( ref $right ne 'HASH' );

    my %merged = %{ $left };
    for my $key ( keys %{ $right } ) {
        if (
            ( ref $right->{$key} eq 'HASH' )
            && ( exists $left->{$key} && ref $left->{$key} eq 'HASH' )
        ) {
            $merged{$key} = __merge_hash( $left->{$key}, $right->{$key} );
        }
        else {
            $merged{$key} = $right->{$key};
        }
    }

    return \%merged;
}

# ------------------------------------ #

1;

package minish::plugin;

sub new {
    my ( $class, %args ) = @_;

    my $config = delete $args{'config'} || {};
    my $self = bless {
        config => $config,
    }, $class;

    return $self;
}

sub config  { $_[0]->{'config'} }
sub setup   { 1 }
sub start   { 1 }

package minish::date;

use Carp ();
use Time::localtime;

our %month2num = (  nil => '00', Jan => '01', Feb => '02', Mar => '03',
                    Apr => '04', May => '05', Jun => '06', Jul => '07',
                    Aug => '08', Sep => '09', Oct => '10', Nov => '11', Dec => '12' );
our @num2month = sort { $month2num{$a} <=> $month2num{$b} } keys %month2num;

sub new {
    my ( $class, %args ) = @_;
    my $epoch = delete $args{'epoch'}
        or Carp::croak "Argument 'epoch' is not specified.";

    my $ctime = ctime($epoch);
    my ( $dw, $mo, $da, $hr, $min, $sec, $yr )
        = ( $ctime =~ m{(\w{3})[ ]+(\w{3})[ ]+(\d{1,2})[ ]+(\d{2}):(\d{2}):(\d{2})[ ]+(\d{4})$} );
    $da = sprintf('%02d', $da);
    my $mo_num = $month2num{$mo};

    my $self = bless {
        epoch   => $epoch,
        yr      => $yr,
        mo      => $mo,
        mo_num  => $mo_num,
        da      => $da,
        hr      => $hr,
        min     => $min,
        sec     => $sec,
        dw      => $dw,
    }, $class;
}

for my $method ( qw( epoch yr mo mo_num da hr min sec dw ) ) {
    no strict 'refs';
    *{$method} = sub {
        return $_[0]->{$method};
    }
}

package minish::page;

use Carp ();
use File::stat;

sub new {
    my ( $class, %args ) = @_;

    my $dir     = delete $args{'dir'}               or Carp::croak "Argument 'dir' is not specified.";
    my $fn      = delete $args{'filename'}          or Carp::croak "Argument 'filename' is not specified.";
    my $ext     = delete $args{'file_extension'}    or Carp::croak "Argument 'file_extension' is not specified.";
    my $prefix  = delete $args{'meta_prefix'} || 'meta-';

    $dir =~ s{/*$}{};
    $fn  =~ s{^/*|/*$}{};
    $ext =~ s{^[.]}{};

    my $self = bless {
        dir             => $dir,
        filename        => $fn,
        file_extension  => $ext,
        title           => q{},
        body            => q{},
        body_source     => q{},
        meta            => {},
        meta_prefix     => $prefix,
        loaded          => 0,
    }, $class;
}

sub loaded          { $_[0]->{'loaded'}         }
sub dir             { $_[0]->{'dir'}            }
sub filename        { $_[0]->{'filename'}       }
sub file_extension  { $_[0]->{'file_extension'} }

sub path {
    my ( $self ) = @_;

    my $dir = $self->dir;
    my $fn  = $self->filename;
    my $ext = $self->file_extension;

    $dir =~ s{/*$}{}g;
    $fn  =~ s{^/*|/*$}{}g;

    return "${dir}/${fn}.${ext}";
}

for my $prop ( qw( title body body_source meta ) ) {
    no strict 'refs';
    *{$prop} = sub {
        my $self = shift;

        if ( $self->exists && ! $self->loaded ) {
            $self->load;
        }

        if ( @_ ) {
            $self->{$prop} = shift;
        }
        else {
            return $self->{$prop};
        }
    }
}

sub date    {
    my ( $self ) = @_;

    my $stat = stat($self->path);
    my $mtime = ( !! $stat ) ? $stat->mtime : time ;

    return minish::date->new( epoch => $mtime );
}

sub exists {
    my ( $self ) = @_;

    if ( -e $self->path && -r _ ) {
        return 1;
    }
    return;
}

sub load {
    my ( $self ) = @_;

    if ( $self->exists ) {
        my ( $title, $body, $meta )
            = ( q{}, q{}, {} );

        open( my $fh, '<', $self->path )
            or Carp::croak "Failed to open file: " . $self->path . ": $!";
        $title = <$fh>;
        chomp $title;
        $body  = do { local $/; <$fh> };
        close($fh);

        ( $meta, $body ) = $self->parse_meta( $body );

        $self->{'title'}        = $title;
        $self->{'body'}         = $body;
        $self->{'body_source'}  = $body;
        $self->{'meta'}         = $meta;
        $self->{'loaded'}       = 1;

        return 1;
    }
    else {
        Carp::croak "File does not exists: ". $self->path;
    }
}

sub parse_meta {
    my ( $self, $content ) = @_;

    my @lines = split m{\n}, $content;
    my $prefix = quotemeta( $self->{'meta_prefix'} );

    my $meta = {};
    my $body = q{};

    while ( defined(my $line = shift @lines) ) {
        if ( $line !~ m{^$prefix} ) {
            $body = join qq{\n}, $line, @lines;
            last;
        }
        if ( $line =~ m{^$prefix(\w+?)[:]\s*(.*)\s*} ) {
            $meta->{$1} = $2;
        }
    }

    return ( $meta, $body );
}

sub textize {
    my ( $self ) = @_;

    my $text = q{};
       $text .= $self->title . "\n";

    my $prefix = $self->{'meta_prefix'};
    for my $prop ( sort keys %{ $self->meta } ) {
        $text .= "${prefix}${prop}: " . $self->meta->{$prop} . "\n";
    }

    $text .= $self->body_source;

    return $text;
}

sub save {
    my ( $self ) = @_;

    my $path = $self->path;

    open( my $fh, '>', $path )
        or Carp::croak "Failed to open file: " . $self->path . ": $!";

    my $ret = print $fh $self->textize;
    close($fh);

    return $ret;
}

1;

package minish::pages;

use Carp ();
use File::Find ();

sub new {
    my ( $class, %args ) = @_;

    my $dir = delete $args{'dir'}
        or Carp::croak "Argument 'dir' is not specified.";
    my $fe  = delete $args{'file_extension'}
        or Carp::croak "Argument 'file_extension' is not specified.";
    my $prefix = delete $args{'meta_prefix'};

    $dir =~ s{/*$}{}g;
    $fe  =~ s{^[.]}{}g;

    my $self = bless {
        dir             => $dir,
        file_extension  => $fe,
        meta_prefix     => $prefix
    }, $class;

    return $self;
}

sub dir             { $_[0]->{'dir'}            }
sub file_extension  { $_[0]->{'file_extension'} }
sub meta_prefix     { $_[0]->{'meta_prefix'}    }

sub page {
    my ( $self, $filename ) = @_;

    $filename =~ s{^/*}{}g;
    $filename .= "index" if ( $filename =~ m{/$} );

    return minish::page->new(
        dir             => $self->dir,
        filename        => $filename,
        file_extension  => $self->file_extension,
        meta_prefix     => $self->meta_prefix,
    );
}

sub pages {
    my ( $self ) = @_;

    my %args = (
        dir             => $self->dir,
        file_extension  => $self->file_extension,
        meta_prefix     => $self->meta_prefix,
    );

    my @ret;

    my $dir             = quotemeta($self->dir);
    my $file_extension  = quotemeta($self->file_extension);

    File::Find::find(
        sub {
            my $path = $File::Find::name;


            return if ( -d $path );
            return if ( ! -r $path );

            if ( $path =~ m{^$dir/([^.]+)\.$file_extension$} ) {
                push @ret, $self->page( $1 );
            }
        },
        $self->dir,
    );

    @ret = sort { $a->filename cmp $b->filename } @ret;

    return \@ret;
}

1;

package main;

if ( minish->env_value('bootstrap') ) {
    minish->setup;

    my $app = minish->new;
    my $engine = $app->config->{'global'}->{'engine'} || 'CGI';

    if ( $engine eq 'CGI' ) {
        require CGI;
        $app->cgi( CGI->new );
        $app->prepare;
        $app->run;
    }
    elsif ( $engine eq 'FastCGI' )  {
        require CGI::Fast;
        while ( my $cgi = CGI::Fast->new ) {
            $app->cgi( $cgi );
            $app->prepare;
            $app->run;
        }
    }
}

1;
__DATA__
__END__

=head1 NAME

minish - blosxom like CMS

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>thotep@nyarla.netE<gt>

=head1 LICENSE

Copyright (c) 2009 Naoki Okamura (Nyarla) thotep@nyarla.net

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

=cut
