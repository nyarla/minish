#!perl

use strict;
use warnings;

my $basedir = '/path/to/basedir';

return {
    global      => {
        url                 => '',
        plugins_dir         => "${basedir}/plugins",
        plugin_state_dir    => "${basedir}/state"
    },

    variables   => {},

    flavour     => {
        dir                 => "${basedir}/flavours",
        default             => 'html',
    },

    pages       => {
        dir             => "${basedir}/pages",
        file_extension  => 'txt',
    },

    plugins     => [
        # { plugin => $plugin, config => \%config },
    ],
};