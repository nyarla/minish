#!perl

use strict;
use warnings;

use t::Util qw( $script plugin );
use Test::More;
use File::Temp qw( tempdir );

my ( $path, $class ) = @{ plugin('amazon') }{qw( path class )};

require $script;
require $path;

my $apikey = q{};
my $secret = q{};
my $prefix = q{MINISH_PLUGIN_AMAZON};
if ( exists $ENV{"${prefix}_APIKEY"} && exists $ENV{"${prefix}_SECRET"} ) {
    $apikey = $ENV{"${prefix}_APIKEY"};
    $secret = $ENV{"${prefix}_SECRET"};
}
else {
    plan skip_all => "${prefix}_APIKEY or ${prefix}_SECRET is not specified";
}

plan tests => 2 + 4 + 6 + 6;

my $plugin = $class->new( config => {
    apikey  => $apikey,
    secret  => $secret,
    asoid   => 'example-22',
    locale  => 'jp',
} );
my $tmpdir = tempdir( CLANUP => 1 );

minish->merge_config({
    global => {
        plugin_state_dir => $tmpdir,
    },
});

$plugin->setup('minish');

my $asin = 4061826263;
my $cache = "${tmpdir}/${asin}.xml";

$plugin->request( $asin => $cache );
my $ret = $plugin->parse_cache( $cache );

is(     $ret->{'ASIN'},                4061826263 );
like(   $ret->{'DetailPageURL'},        qr{^http://www\.amazon\.co\.jp/} );

like(   $ret->{'AddToWishlist'},        qr{^http://www\.amazon\.co\.jp/} );
like(   $ret->{'TellAFriend'},          qr{^http://www\.amazon\.co\.jp/} );
like(   $ret->{'AllCustomerReviews'},   qr{^http://www\.amazon\.co\.jp/} );
like(   $ret->{'AllOffers'},            qr{^http://www\.amazon\.co\.jp/} );

like(   $ret->{'SwatchImage'},          qr{^http://ecx\.images-amazon\.com/images/I/} );
like(   $ret->{'SmallImage'},           qr{^http://ecx\.images-amazon\.com/images/I/} );
like(   $ret->{'ThumbnailImage'},       qr{^http://ecx\.images-amazon\.com/images/I/} );
like(   $ret->{'TinyImage'},            qr{^http://ecx\.images-amazon\.com/images/I/} );
like(   $ret->{'MediumImage'},          qr{^http://ecx\.images-amazon\.com/images/I/} );
like(   $ret->{'LargeImage'},           qr{^http://ecx\.images-amazon\.com/images/I/} );

ok(     $ret->{'Author'}        );
ok(     $ret->{'Binding'}       );
ok(     $ret->{'ListPrice'}     );
ok(     $ret->{'Manufacturer'}  );
ok(     $ret->{'ReleaseDate'}   );
ok(     $ret->{'Title'}         );
