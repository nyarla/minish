#!perl

use strict;
use warnings;

use t::Util qw( $script plugin );
use Test::More tests => 2;
use File::Temp qw( tempdir );

my ( $path, $class ) = @{ plugin('amazon') }{qw( path class )};

require $script;
require $path;

my $plugin = $class->new( config => {
    apikey  => 'XXXXXXXXXXXXXXXXXXXX',
    secret  => 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
    asoid   => 'example-22',
    locale  => 'jp',
} );

my $tmpdir = tempdir( CLANUP => 1 );

minish->merge_config({
    global => {
        plugin_state_dir => $tmpdir,
    },
});

ok( $plugin->setup('minish') );

is_deeply(
    $plugin->{'stash'},
    {
        apikey      => 'XXXXXXXXXXXXXXXXXXXX',
        secret      => 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        asoid       => 'example-22',
        url         => 'http://ecs.amazonaws.jp/onca/xml',
        expire      => 24 * 7,
        cache_dir   => "$tmpdir/amazon",
        lastreq     => "$tmpdir/amazon_last_req",
    },
);
