#!perl

use strict;
use warnings;

use t::Util qw( $script $example plugin );
use Test::More;
use File::Temp qw( tempdir );

my ( $path, $class ) = @{ plugin('amazon') }{qw( path class )};

require $script;
require $path;

my $apikey = q{};
my $secret = q{};
my $prefix = q{MINISH_PLUGIN_AMAZON};
if ( exists $ENV{"${prefix}_APIKEY"} && exists $ENV{"${prefix}_SECRET"} ) {
    $apikey = $ENV{"${prefix}_APIKEY"};
    $secret = $ENV{"${prefix}_SECRET"};
}
else {
    plan skip_all => "${prefix}_APIKEY or ${prefix}_SECRET is not specified";
}

plan tests => 3;

my $tmpdir = tempdir( CLANUP => 1 );

my $minish  = minish->new;
my $plugin = $class->new( config => {
    apikey  => $apikey,
    secret  => $secret,
    asoid   => 'example-22',
    locale  => 'jp',
} );

$minish->merge_config({
    global => {
        plugin_state_dir => $tmpdir,
    },
    pages   => {
        dir => "${example}/plugins/amazon/pages",
    },
    flavour => {
        dir => "${example}/plugins/amazon/flavours",
    },
});
$minish->setup_pages;
$minish->path_arguments([qw( foo )]);
$minish->flavour('html');


$plugin->setup('minish');
$plugin->prepare( $minish );

my $page = $minish->pages->page('notfound');

ok( ! $plugin->page( $minish, $page, \q{} ) );

$page = $minish->pages->page('foo');

ok( $plugin->page( $minish, $page, \q{} ) );

is(
    $page->body,
    q{4061826263 #4061826263},
)