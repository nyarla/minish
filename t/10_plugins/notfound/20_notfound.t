#!perl

use strict;
use warnings;

use t::Util qw( $script $example plugin );
use Test::More tests => 2;

my ( $path, $class ) = @{ plugin('notfound') }{qw( path class )};

require $script;
require $path;

my $minish = minish->new;
my $plugin = $class->new( config => {} );
my $page   = minish::page->new( dir => "${example}/plugins/notfound/pages/", filename => 'bar', file_extension => 'txt' );

$minish->merge_config({
    flavour => {
        dir => "${example}/plugins/notfound/flavours",
    },
});

$minish->path_arguments([qw( foo bar baz )]);
$minish->flavour('html');
$minish->header({});

my $flavour = q{};

$plugin->page( $minish, $page, \$flavour );

is( $minish->header->{'-status'}, '404 Not Found' );
is( $flavour, 'notfound.html' );
