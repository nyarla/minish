#!perl

use strict;
use warnings;

use t::Util qw( $script $example plugin );
use Test::More tests => 3;

my ( $path, $class ) = @{ plugin('notfound') }{qw( path class )};

require $script;
require $path;

my $minish = minish->new;
my $plugin = $class->new( config => { exclude_flavour => 'atom', exclude_path_info => '^foo/bar' } );

my %args   = ( dir => "${example}/plugins/page/", file_extension => 'txt' );
my $page   = minish::page->new( filename => 'foo', %args );

$minish->path_arguments([qw( foo bar baz )]);
$minish->flavour('atom');

ok( ! $plugin->page( $minish, $page, \q{} ) );

$page = minish::page->new( filename => 'bar', %args );

ok( ! $plugin->page( $minish, $page, \q{} ) );

$minish->flavour('html');
$minish->path_info('foo/bar/baz.html');

ok( ! $plugin->page( $minish, $page, q{} ) );
