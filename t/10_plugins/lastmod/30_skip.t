#!perl

use strict;
use warnings;

use t::Util qw( $script $example plugin );
use Test::More tests => 5;

my ( $path, $class ) = @{ plugin('lastmod') }{qw( path class )};

require $script;
require $path;

my $minish = minish->new;
my $plugin = $class->new;

$minish->merge_config({
    pages => {
        dir => "${example}/plugins/lastmod/pages",
    },
});
$minish->setup_pages;
$minish->path_arguments([qw( foo bar )]);

ok( ! $plugin->skip( $minish ) );

$minish->path_arguments([qw( foo )]);
$minish->header({});

{
    no warnings;
    package minish::plugin::lastmod;
    *find_files = sub {
        return ( 1893456000 ); # 2030-01-01T00:00:00Z
    }
}

ok( ! $plugin->skip( $minish ) );

is( $minish->header->{'-Last_modified'}, 'Tue, 01 Jan 2030 00:00:00 GMT' ); # 2030-01-01T00:00:00Z

$ENV{'HTTP_IF_MODIFIED_SINCE'} = 'Wed, 01 Jan 2031 00:00:00 GMT'; # 2031-01-01T00:00:00Z

ok( $plugin->skip( $minish ) );

is_deeply(
    $minish->header,
    {
        -Last_modified  => 'Tue, 01 Jan 2030 00:00:00 GMT',
        -status         => '304 Not Modified',
    },
);
