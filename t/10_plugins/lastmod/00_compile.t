#!perl

use strict;
use warnings;

use t::Util qw( $script plugin );
use Test::More tests => 1;

BEGIN {
    require $script;
    require_ok( plugin('lastmod')->{'path'} );
}
