#!perl

use strict;
use warnings;

use t::Util qw( $script plugin );
use Test::More;

eval {
    use HTTP::Request;
    use HTTP::Request::AsCGI;
    use CGI;
};

if ( $@ ) {
    plan skip_all => "HTTP::Request or HTTP::Request::AsCGI or CGI is not installed: $@";
}

plan tests => 3;

my ( $path, $class ) = @{ plugin('lastmod') }{qw( path class )};

require $script;
require $path;

my $minish = minish->new;
my $plugin = $class->new;

{
    my $req = HTTP::Request->new( GET => 'http://localhost/foo/bar.html?foo=bar' );
    my $ctx = HTTP::Request::AsCGI->new($req)->setup;
    my $cgi = CGI->new;

    $minish->cgi( $cgi );

    ok( ! $plugin->start( $minish ) );

}

{
    my $req = HTTP::Request->new( POST => 'http://localhost/foo/bar.html' );
    my $ctx = HTTP::Request::AsCGI->new($req)->setup;
    my $cgi = CGI->new;

    $minish->cgi( $cgi );

    ok( ! $plugin->start( $minish ) );

}

{
    my $req = HTTP::Request->new( GET => 'http://localhost/foo/bar.html' );
    my $ctx = HTTP::Request::AsCGI->new($req)->setup;
    my $cgi = CGI->new;

    $minish->cgi( $cgi );

    ok( $plugin->start( $minish ) );

}