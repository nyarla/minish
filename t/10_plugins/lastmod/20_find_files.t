#!perl

use strict;
use warnings;

use t::Util qw( $script $example plugin );
use Test::More tests => 2;

my ( $path, $class ) = @{ plugin('lastmod') }{qw( path class )};

require $script;
require $path;

my $minish = minish->new;
my $plugin = $class->new;

$minish->merge_config({
    global  => {
        plugins_dir         => "${example}/plugins/lastmod/plugins",
        plugin_state_dir    => "${example}/plugins/lastmod/state",
    },
    flavour => {
        dir => "${example}/plugins/lastmod/flavours",
    },
});

$ENV{'MINISH_CONFIG'} = "${example}/plugins/lastmod/config.pl";

$minish->setup_variable;

my @mtimes = $plugin->find_files( $minish );

ok( scalar(@mtimes) >= 4 );

$plugin = $class->new( config => {
    check_dirs  => "${example}/plugins/lastmod/check",
    check_files => "${example}/plugins/lastmod/config.pl",
} );

@mtimes = $plugin->find_files( $minish );

ok( scalar(@mtimes) >= 2 );
