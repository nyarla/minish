#!perl

use strict;
use warnings;

use t::Util qw( $script $example plugin );
use Test::More tests => 3;

my ( $path, $class ) = @{ plugin('interpolate_page') }{qw( path class )};

require $script;
require $path;

my $minish = minish->new;
my $plugin = $class->new;

$minish->merge_config({
    pages => {
        dir => "${example}/plugins/interpolate_page",
    },
});
$minish->setup_pages;

my $page = $minish->pages->page('notfound');

ok( ! $plugin->page( $minish, $page, \q{} ) );

$page = $minish->pages->page('page');

ok( $plugin->page( $minish, $page, \q{} ) );

is( $page->body, 'title' );
