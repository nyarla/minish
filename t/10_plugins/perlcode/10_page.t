#!perl

use strict;
use warnings;

use t::Util qw( $script $example plugin );
use Test::More tests => 3;

my ( $path, $class ) = @{ plugin('perlcode') }{qw( path class )};

require $script;
require $path;

my $minish = minish->new;
my $plugin = $class->new;

$minish->merge_config({
    pages => {
        dir => "${example}/plugins/perlcode",
    },
});
$minish->setup_pages;

my $page = $minish->pages->page('notfound');

ok( ! $plugin->page( $minish, $page, \q{} ) );

$page = $minish->pages->page('foo');

ok( $plugin->page( $minish, $page, \q{} ) );
is( $page->body, 'foobar' );
