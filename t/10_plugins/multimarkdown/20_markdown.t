#!perl

use strict;
use warnings;

use t::Util qw( $script $example plugin );
use Test::More tests => 2;

my ( $path, $class ) = @{ plugin('multimarkdown') }{qw( path class )};

require $script;
require $path;

my $minish = minish->new;
my $plugin = $class->new( config => { use_meta => 1 } );

$minish->merge_config({
    pages => {
        dir => "${example}/plugins/multimarkdown/",
    },
});

$minish->setup_pages;

my $page   = $minish->pages->page('markdown');

$page->load;
$plugin->setup;
$plugin->page( $minish, $page, \q{} );


my $body = qq{<p>Test Page</p>\n};

is(
    $page->body,
    $body,
);

$plugin = $class->new( config => {} );
$plugin->setup;
$page->load;
$plugin->page( $minish, $page, \q{} );

is(
    $page->body,
    $body,
);
