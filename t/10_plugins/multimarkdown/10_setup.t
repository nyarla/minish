#!perl

use strict;
use warnings;

use t::Util qw( $script plugin );
use Test::More tests => 2;

my ( $path, $class ) = @{ plugin('multimarkdown') }{qw( path class )};

require $script;
require $path;

my $minish  = 'minish';
my $plugin  = $class->new( config => { use_meta => 1 } );

$plugin->setup( $minish );

isa_ok( $plugin->{'markdown'}, 'Text::MultiMarkdown' );
is( $plugin->{'use_meta'}, 1 );

