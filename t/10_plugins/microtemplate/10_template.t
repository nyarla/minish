#!perl

use strict;
use warnings;

use t::Util qw( $script plugin );
use Test::More tests => 2;

my ( $path, $class ) = @{ plugin('microtemplate') }{qw( path class )};

require $script;
require $path;

minish->merge_config({ foo => 'AAA' });
minish->variables({ foo => 'BAR' });

my $minish = minish->new;
my $plugin = $class->new;

can_ok( $plugin, 'interpolate' );

my $sub = $plugin->interpolate( 'minish' );

is(
    $sub->(
        $minish,
        q{<?= $_[0]->config->{'foo'} ?><?= $_[1]->{'foo'}?><?= $_[2]->{'bar'} ?>},
        { bar => 'BAZ' },
    ),
    'AAABARBAZ',
);
