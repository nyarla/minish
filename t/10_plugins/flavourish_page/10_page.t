#!perl

use strict;
use warnings;

use t::Util qw( $script $example plugin );
use Test::More tests => 1 + 1 + 2 + 3;

my ( $path, $class ) = @{ plugin('flavourish_page') }{qw( path class )};

require $script;
require $path;

my $minish = minish->new;
my $plugin = $class->new(
    config => {
        enable_path     => qr{^foo},
        stop_path       => qr{^foo/bar$},
    }
);

$minish->merge_config({
    pages => {
        dir => "${example}/plugins/flavourish_page/",
    }
});
$minish->setup_pages;

# exists page
my $page = $minish->pages->page('foo');
ok( ! $plugin->page( $minish, $page, \q{} ) );

# no enable path
$minish->path_info('disable.html');
ok( ! $plugin->page( $minish, $page, \q{} ) );

# stop path
$minish->path_arguments([qw( foo bar baz )]);
$minish->path_info('foo/bar/baz');
$page = $minish->pages->page('foo/bar/baz');

ok( ! $plugin->page( $minish, $page, \q{} ) );
is( $page->filename, 'foo/bar/index' );

# flavourish page
$minish->path_arguments([qw( foo baz bar )]);
$minish->path_info('foo/baz/bar.html');
$page = $minish->pages->page('foo/baz/bar');

ok( $plugin->page( $minish, $page, \q{} ) );
is( $page->filename, 'foo/index' );
is( $page->loaded, 1 );
