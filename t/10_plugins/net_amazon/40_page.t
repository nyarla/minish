#!perl

use strict;
use warnings;

use t::Util qw( $script $example plugin );
use Test::More;
use File::Temp qw( tempdir );

my ( $path, $class ) = @{ plugin('net_amazon') }{qw( path class )};

require $script;
require $path;

my $apikey = q{};
my $secret = q{};
my $prefix = q{MINISH_PLUGIN_NET_AMAZON};
if ( exists $ENV{"${prefix}_APIKEY"} && exists $ENV{"${prefix}_SECRET"} ) {
    $apikey = $ENV{"${prefix}_APIKEY"};
    $secret = $ENV{"${prefix}_SECRET"};
}
else {
    plan skip_all => "${prefix}_APIKEY or ${prefix}_SECRET is not specified";
}


plan tests => 3;

my $tmpdir = tempdir( CLEANUP => 1 );

minish->merge_config({
    global => {
        plugin_state_dir => $tmpdir,
    },
    flavour => {
        dir => "${example}/plugins/net_amazon/flavours",
    },
    pages   => {
        dir => "${example}/plugins/net_amazon/pages",
    }
});

my $minish = minish->new;
   $minish->setup_pages;
   $minish->path_arguments([qw( foo )]);
   $minish->flavour('html');

my $plugin = $class->new( config => {
    amazon => {
        token       => $apikey,
        secret_key  => $secret,
        locale      => 'jp',
    },
} );
$plugin->setup('minish');
$plugin->prepare( $minish );

my $page = $minish->pages->page('notfound');

ok( ! $plugin->page( $minish, $page, \q{} ) );

$page = $minish->pages->page('foo');

ok( $plugin->page( $minish, $page, \q{} ) );

is(
    $page->body,
    q{4061826263 #4061826263},
)