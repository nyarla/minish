#!perl

use strict;
use warnings;

use t::Util qw( $script plugin );
use Test::More tests => 3;
use File::Temp qw( tempdir );

my ( $path, $class ) = @{ plugin('net_amazon') }{qw( path class )};

require $script;
require $path;

my $tmpdir = tempdir( CLEANUP => 1 );

minish->merge_config({
    global => {
        plugin_state_dir => $tmpdir,
    },
});

my $plugin = $class->new( config => {
    amazon => {
        token   => 'XXXXXXXXXXXXXXXXXXXX',
        locale  => 'jp',
    },
} );

ok( $plugin->setup('minish') );

isa_ok( $plugin->{'ua'}, 'Net::Amazon' );

is( $plugin->{'lastreq'}, "${tmpdir}/net_amazon_lastreq" );
