#!perl

use strict;
use warnings;

use t::Util qw( $script plugin );
use Test::More;
use File::Temp qw( tempdir );

my ( $path, $class ) = @{ plugin('net_amazon') }{qw( path class )};

require $script;
require $path;

my $apikey = q{};
my $secret = q{};
my $prefix = q{MINISH_PLUGIN_NET_AMAZON};
if ( exists $ENV{"${prefix}_APIKEY"} && exists $ENV{"${prefix}_SECRET"} ) {
    $apikey = $ENV{"${prefix}_APIKEY"};
    $secret = $ENV{"${prefix}_SECRET"};
}
else {
    plan skip_all => "${prefix}_APIKEY or ${prefix}_SECRET is not specified";
}

plan tests => 2;

my $tmpdir = tempdir( CLEANUP => 1 );

minish->merge_config({
    global => {
        plugin_state_dir => $tmpdir,
    },
});

my $plugin = $class->new( config => {
    amazon => {
        token       => $apikey,
        secret_key  => $secret,
        locale      => 'jp',
    },
} );

$plugin->setup('minish');

my $asin = 4061826263;
my $res  = $plugin->request( asin => $asin );

isa_ok( $res, 'Net::Amazon::Response::ASIN' );

is( ($res->properties)[0]->Asin, $asin );
