#!perl

package main;

use strict;
use warnings;

use t::Util qw( $script plugin );
use Test::More tests => 3 + 3 + 6 + 3;
use File::Temp qw( tempdir );

my ( $path, $class ) = @{ plugin('net_amazon') }{qw( path class )};

require $script;
require $path;

package Cache; 1;
package Cache::File;

our @ISA = qw( Cache );

sub new {
    my ( $class, %args ) = @_;
    bless { %args }, $class
}

1;

package Cache::Cache; 1;
package Cache::FileCache;

our @ISA = qw( Cache::Cache );

sub new {
    my ( $class, $args ) = @_;
    bless $args, $class
}
1;

package CHI;

sub new {
    my ( $class, %args ) = @_;
    bless { %args }, $class;
}

1;

package TestCache;

sub new {
    my ( $class, %args ) = @_;
    bless { %args }, $class;
}

package main;

my $tmpdir     = tempdir( CLEANUP => 1 );

minish->merge_config({
    global => {
        plugin_state_dir => $tmpdir,
    },
});

my %args = (
    amazon => {
        token   => 'XXXXXXXXXXXXXXXXXXXX',
        locale  => 'jp',
    },
);

my $plugin = $class->new( config => {
    %args,
    use_cache => 1,
    cache => {
        class   => 'Cache::File',
        args    => {},
    },
} );

ok( $plugin->setup('minish') );

isa_ok( $plugin->{'ua'}, 'Net::Amazon' );
is_deeply(
    $plugin->{'ua'}->{'cache'},
    Cache::File->new(
        cache_root => "$tmpdir/net_amazon_cache",
    )
);

$plugin = $class->new( config => {
    %args,
    use_cache => 1,
    cache => {
        class   => 'Cache::FileCache',
        args    => {},
    },
} );

ok( $plugin->setup('minish') );

isa_ok( $plugin->{'ua'}, 'Net::Amazon' );
is_deeply(
    $plugin->{'ua'}->{'cache'},
    Cache::FileCache->new({
        cache_root => "$tmpdir/net_amazon_cache",
    }),
);

$plugin = $class->new( config => {
    %args,
    use_cache => 1,
    cache => {
        class   => 'CHI',
        args    => {
            driver => 'File'
        },
    },
} );

ok( $plugin->setup('minish') );

isa_ok( $plugin->{'ua'}, 'Net::Amazon' );
is_deeply(
    $plugin->{'ua'}->{'cache'},
    CHI->new(
        driver => 'File',
        root_dir => "$tmpdir/net_amazon_cache",
    ),
);

$plugin = $class->new( config => {
    %args,
    use_cache => 1,
    cache => {
        class   => 'CHI',
        args    => {
            driver => 'FastMmap'
        },
    },
} );

ok( $plugin->setup('minish') );

isa_ok( $plugin->{'ua'}, 'Net::Amazon' );
is_deeply(
    $plugin->{'ua'}->{'cache'},
    CHI->new(
        driver => 'FastMmap',
        root_dir => "$tmpdir/net_amazon_cache",
    ),
);

$plugin = $class->new( config => {
    %args,
    use_cache => 1,
    cache => {
        class   => 'TestCache',
        deref   => 1,
        args    => {
            foo => 'BAR',
        },
    },
} );

ok( $plugin->setup('minish') );

isa_ok( $plugin->{'ua'}, 'Net::Amazon' );
is_deeply(
    $plugin->{'ua'}->{'cache'},
    TestCache->new(
        foo => 'BAR',
    ),
);