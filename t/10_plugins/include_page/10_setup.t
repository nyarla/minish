#!perl

use strict;
use warnings;

use t::Util qw( $script $example plugin );
use Test::More tests => 5;

my ( $path, $class ) = @{ plugin('include_page') }{qw( path class )};

require $script;
require $path;

my $minish = minish->new;
my $plugin = $class->new;

$minish->merge_config({
    pages => {
        dir => "${example}/plugins/include_page/pages",
    },
    flavour => {
        dir => "${example}/plugins/include_page/flavours",
    }
});
$minish->setup_pages;
$minish->path_arguments([qw( foo )]);
$minish->flavour('html');

# setup
ok( $plugin->prepare( $minish ) );

# set variables
isa_ok( minish->variables->{'include'}, 'CODE' );

# include
our @ret;
{
    no warnings;
    package minish;
    *run_plugins = sub {
        my ( $app, $method, @args ) = @_;
        push @ret, [ $method, @args ];
    }
}

is(
    $minish->variables->{'include'}->('foo'),
    'body',
);

is(
    $minish->variables->{'include'}->('foo', 'bar'),
    'barbody',
);

my $page = $minish->pages->page('foo');
   $page->load;

is_deeply(
    [ @ret ],
    [
        [ 'page', $page, \q{<% $vars->{'page'}->body %>} ],
        [ 'page', $page, \q{bar<% $vars->{'page'}->body %>} ],
    ]
)