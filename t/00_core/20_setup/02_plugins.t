#!perl

use strict;
use warnings;

use t::Util qw( $script $example );
use Test::More tests => 3;

require $script;

minish->config({
    global => {
        plugins_dir => ["$example/core/setup_plugins"],
    },
    plugins => [
        { plugin => 'foo', config => { foo => 'BAR' } },
        { plugin => 'bar', config => { foo => 'BAR' } },
    ],
});

minish->setup_plugins;

is_deeply(
    minish->plugins,
    [
        {
            plugin => {
                config => {
                    foo => 'BAR',
                },
                class => 'minish',
            },
            on_off => 1,
        },
        {
            plugin => {
                config => {
                    foo => 'BAR',
                },
                class => 'minish',
            },
            on_off => -1,
        },
    ],
);

isa_ok( minish->plugins->[0]->{'plugin'}, 'minish::plugin::foo' );
isa_ok( minish->plugins->[1]->{'plugin'}, 'minish::plugin::bar' );
