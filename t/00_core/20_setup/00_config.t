#!perl

use strict;
use warnings;

use t::Util qw( $script $example );
use Test::More tests => 1;

require $script;

local $ENV{'MINISH_CONFIG'} = "$example/core/setup_config.pl";

my %config = %{ minish->config };

minish->setup_config;

is_deeply(
    minish->config,
    {
        %config,
        foo => 'bar',
    },
);
