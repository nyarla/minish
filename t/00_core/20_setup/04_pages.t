#!perl

use strict;
use warnings;

use t::Util qw( $script );
use Test::More tests => 1;

require $script;

minish->merge_config({
    pages => {
        dir         => "/path/to/pages",
        meta_prefix => '@',
    },
});

minish->setup_pages;

is_deeply(
    minish->pages,
    minish::pages->new(
        dir             => "/path/to/pages",
        file_extension  => 'txt',
        meta_prefix     => '@',
    ),
);
