#!perl

use strict;
use warnings;

use t::Util qw( $script );
use Test::More tests => 2;

require $script;

minish->config({
    global => {
        plugins_dir => '/path/to/plugins/',
        plugin_state_dir => '/path/to/state//',
    },
    flavour => {
        dir => '/path/to/flavour/dir///',
    },
    pages   => {
        dir => '/path/to/pages///////',
    },
    variables => {
        foo => 'BAR',
    }
});

minish->setup_variable;

is_deeply(
    minish->config,
    {
        global => {
            plugins_dir => [qw(/path/to/plugins)],
            plugin_state_dir => '/path/to/state',
        },
        flavour => {
            dir => '/path/to/flavour/dir',
        },
        pages   => {
            dir => '/path/to/pages',
        },
        variables => {
            foo => 'BAR',
        }
    },
);

is_deeply(
    minish->variables,
    {
        foo => 'BAR',
    }
)