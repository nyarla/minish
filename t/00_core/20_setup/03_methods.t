#!perl

use strict;
use warnings;

our $function   = sub { 'FOO' };
our @methods    = qw( template interpolate );

package plugin;
sub new { bless {}, shift }

for my $method ( @main::methods ) {
    no strict 'refs';
    *{$method} = sub { return $main::function }
}

1;

package main;

use t::Util qw( $script );
use Test::More;

plan tests => scalar(@methods) * 2;

require $script;

minish->plugins( [ { plugin => plugin->new, on_off => 1 } ] );
minish->setup_methods;

for my $method ( @methods ) {
    isa_ok( minish->methods->{$method}, 'CODE' );
    is( minish->methods->{$method}, $function );
}
