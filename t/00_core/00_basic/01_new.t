#!perl

use strict;
use warnings;

use t::Util qw( $script );
use Test::More tests => 1;

require $script;

isa_ok( minish->new, 'minish' );
