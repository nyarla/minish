#!perl

use strict;
use warnings;

use t::Util qw( $script );
use Test::More tests => 1;

BEGIN { require_ok( $script ) }
