#!perl

use strict;
use warnings;

use t::Util qw( $script );
use Test::More;

require $script;

my @methods = qw( cgi url path_info path_arguments flavour header output );

plan tests => scalar(@methods) * 2;

for my $method ( @methods ) {
    can_ok('minish', $method);
    minish->$method( [] );
    isa_ok( minish->$method, 'ARRAY' );
}