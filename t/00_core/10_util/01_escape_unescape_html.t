#!perl

use strict;
use warnings;

use t::Util qw( $script );
use Test::More;

require $script;

my %escape = (
    '&' => '&amp;',
    '<' => '&lt;',
    '>' => '&gt;',
    '"' => '&quot;',
    "'" => '&apos;',
);

plan tests => scalar( keys %escape ) * 2;

for my $target ( keys %escape ) {
    is( minish->escape_html($target), $escape{$target} );
    is( minish->unescape_html($escape{$target}),  $target );
}