#!perl

use strict;
use warnings;

use t::Util qw( $script );
use Test::More tests => 1;

require $script;

my $left = {
    foo => 'bar',
    bar => [
        'BAR',
        'BAZ',
    ],
    baz => {
        AAA => 'AAA',
        BBB => 'BBB',
    }
};

my $right = {
    bar => 'FOO',
    baz => {
        CCC => 'CCC',
        DDD => 'DDD',
    },
};

is_deeply(
    minish::__merge_hash( $left, $right ),
    {
        foo => 'bar',
        bar => 'FOO',
        baz => {
            AAA => 'AAA',
            BBB => 'BBB',
            CCC => 'CCC',
            DDD => 'DDD',
        },
    },
);
