#!perl

use strict;
use warnings;

use t::Util qw( $script );
use Test::More tests => 5;

require $script;

my $plugin = minish::plugin->new( config => { foo => 'BAR' } );

isa_ok( $plugin, 'minish::plugin' );

can_ok( $plugin, 'config' );
can_ok( $plugin, 'setup' );
can_ok( $plugin, 'start' );

is_deeply(
    $plugin->config,
    {
        foo => 'BAR',
    },
);

