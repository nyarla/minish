#!perl

use strict;
use warnings;

use t::Util qw( $script );
use Test::More tests => 2;

require $script;

minish->merge_config({ global => { plugin_state_dir => '/path/to/state' } });

is(
    minish->plugin_state('/foo/bar'),
    '/path/to/state/foo/bar',
);

is(
    minish->plugin_state('foo/bar'),
    '/path/to/state/foo/bar',
);
