#!perl

use strict;
use warnings;

package pluginA;

sub new { bless {}, shift }

sub foo {
    my ( $self, $app, @args ) = @_;
    $self->{'app'}  = $app,
    $self->{'args'} = \@args;
    return 'FOO'
}

package pluginB;

sub new { bless {}, shift }

sub bar {
    my ( $self, $app, @args ) = @_;
    $self->{'app'}  = $app,
    $self->{'args'} = \@args;

    return 'BAR'
}

package main;

use t::Util qw( $script );
use Test::More tests => 3;

require $script;

my $pluginA = pluginA->new;
my $pluginB = pluginB->new;

minish->plugins( [
    { plugin => $pluginA, on_off => 1 },
    { plugin => $pluginB, on_off => -1 },
] );

is( minish->run_plugin_first('foo'), 'FOO' );
is( minish->run_plugin_first('bar'), undef );

is_deeply(
    $pluginA,
    {
        app     => 'minish',
        args    => [],
    },
);
