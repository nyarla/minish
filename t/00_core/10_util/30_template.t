#!perl

use strict;
use warnings;

use t::Util qw( $script $example );
use Test::More tests => 2;

require $script;

minish->config({
    flavour => {
        dir => "$example/core/util_template",
    },
});

is(
    minish->template('/foo/bar', 'head', 'html'),
    'FOO',
);

is(
    minish->template('foo/bar/baz', 'foot', 'html'),
    'BAR',
);
