#!perl

use strict;
use warnings;

use t::Util qw( $script );
use Test::More;

require $script;

my @methods = qw( epoch yr mo mo_num da hr min sec dw );

plan tests => 1 + scalar(@methods) + 8;

my $date = minish::date->new( epoch => 1236396060 ); # 2009-03-07 12:21:00 (Sat)

isa_ok( $date, 'minish::date' );
for my $method ( @methods ) {
    can_ok( $date, $method );
}

is( $date->yr,      '2009'  );
is( $date->mo,      'Mar'   );
is( $date->mo_num,  '03'    );
is( $date->da,      '07'    );
is( $date->hr,      '12'    );
is( $date->min,     '21'    );
is( $date->sec,     '00'    );
is( $date->dw,      'Sat'   );
