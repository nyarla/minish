#!perl

use strict;
use warnings;

use t::Util qw( $script $example );
use Test::More tests => 4 + 5 + 1;

require $script;

can_ok( 'minish::pages', qw( new dir file_extension meta_prefix ) );

my $pages = minish::pages->new(
    dir             => "${example}/core/util_minish_pages/",
    file_extension  => '.txt',
    meta_prefix     => '@',
);

isa_ok( $pages,                 'minish::pages'                     );
is(     $pages->dir,            "${example}/core/util_minish_pages" );
is(     $pages->file_extension, 'txt'                               );

my $page = $pages->page('/foo/');

isa_ok( $page,                  'minish::page'                      );
is(     $page->dir,             "${example}/core/util_minish_pages" );
is(     $page->filename,        "foo/index"                         );
is(     $page->file_extension,  'txt'                               );
is(     $page->{'meta_prefix'}, '@'                                 );

is_deeply(
    $pages->pages,
    [
        $pages->page('bar'),
        $pages->page('baz'),
        $pages->page('foo'),
    ],
);