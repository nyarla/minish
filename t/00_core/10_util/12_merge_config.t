#!perl

use strict;
use warnings;

use t::Util qw( $script );
use Test::More tests => 1;

require $script;

minish->config( { foo => 'BAR' } );

minish->merge_config( { bar => 'BAZ' } );

is_deeply(
    minish->config,
    {
        foo => 'BAR',
        bar => 'BAZ',
    },
);
