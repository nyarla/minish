#!perl

use strict;
use warnings;

use t::Util qw( $script );
use Test::More tests => 2;

require $script;

local $ENV{'MINISH_FOO'} = 'bar';

can_ok( 'minish', 'env_value' );

is( minish->env_value('foo'), 'bar' );
