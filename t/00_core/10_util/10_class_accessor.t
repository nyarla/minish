#!perl

use strict;
use warnings;

use t::Util qw( $script );
use Test::More;

require $script;

my @keys = qw( config plugins methods variables pages );

plan tests => scalar(@keys) * 2;

for my $name ( @keys ) {
    can_ok( 'minish', $name );
    minish->$name( {} );
    isa_ok( minish->$name, 'HASH' );
}
