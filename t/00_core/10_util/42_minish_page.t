#!perl

use strict;
use warnings;

use t::Util qw( $script $example );
use Test::More tests => 2 + 6 + 1 + 4 + 2;
use File::Temp;

require $script;

my $dir         = "${example}/core";
my $filename    = "util_minish_page";
my $file_ext    = 'txt';
my $path        = "${dir}/${filename}.${file_ext}";

my $page = minish::page->new(
    dir             => $dir,
    filename        => $filename,
    file_extension  => $file_ext,
);

isa_ok( $page, 'minish::page' );
can_ok( $page, qw( path dir filename file_extension title body body_source meta date exists loaded load save parse_meta textize ) );

is(     $page->dir,             $dir            );
is(     $page->filename,        $filename       );
is(     $page->file_extension,  $file_ext       );
is(     $page->path,            $path           );
isa_ok( $page->date,            'minish::date'  );
is(     $page->loaded,          0               );

is_deeply(
    [$page->parse_meta(
qq{meta-foo: bar
meta-bar: baz
Content}
    )],
    [
        { foo => 'bar', bar => 'baz' },
        'Content',
    ]
);

is( $page->title, "Test Page" );
is( $page->body,  "\nTest Page" );
is( $page->body_source, "\nTest Page" );
is_deeply(
    $page->meta,
    {
        foo => 'BAR',
    },
);

my $text = qq{Test Page\nmeta-foo: BAR\n\nTest Page};

is(
    $page->textize,
    $text,
);

{
    my $tmp = File::Temp->new( UNLINK => 1 );

    no warnings;
    package minish::page;
    *path = sub {
        return $tmp->filename;
    };
    package main;
    
    $page->save;
    ok( do { local $/; <$tmp> }  );


}

