#!perl

use strict;
use warnings;

package pluginA;

sub new { bless {}, shift };

sub foo {
    my ( $self, $app, @args ) = @_;
    $self->{'app'}  = $app,
    $self->{'args'} = \@args;
    return 'FOO'
}

package pluginB;

use base qw( pluginA );

sub foo {
    return 'BAR';
}

package main;

use t::Util qw( $script );
use Test::More tests => 2;

require $script;

my $pluginA = pluginA->new;
my $pluginB = pluginB->new;

minish->plugins( [
    { plugin => $pluginA, on_off => 1 },
    { plugin => $pluginB, on_off => -1 },
] );

is_deeply(
    [ minish->run_plugins('foo', bar => 'baz') ],
    [ qw( FOO ) ],
);

is_deeply(
    $pluginA,
    {
        app     => 'minish',
        args    => [qw( bar baz )],
    }
);
