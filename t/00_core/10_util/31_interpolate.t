#!perl

use strict;
use warnings;

use t::Util qw( $script );
use Test::More tests => 1;

require $script;

minish->variables({ foo => '<BAR>' });

my $tmpl = q{
    <% $global->{'foo'} %>
    <$ $global->{'foo'} $>
    <% $vars->{'bar'} %>
    <$ $vars->{'bar'} $>
};


is(
    minish->interpolate( $tmpl, { bar => '<BAZ>' } ),
    qq{
    &lt;BAR&gt;
    <BAR>
    &lt;BAZ&gt;
    <BAZ>
},
);

