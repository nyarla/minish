#!perl

use strict;
use warnings;

use t::Util qw( $script );
use Test::More tests => 4;

require $script;

{
    no strict 'refs';
    no warnings 'redefine';
    package minish;
    for my $method (qw( config variable plugins methods )) {
        *{"minish::setup_${method}"} = sub {
            package main;
            ok(1);
        };
    }
};

minish->setup;
