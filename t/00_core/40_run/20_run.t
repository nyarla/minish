#!perl

use strict;
use warnings;

use t::Util qw( $script $example );
use Test::More;

eval {
    use HTTP::Request;
    use HTTP::Request::AsCGI;
    use CGI;
};

if ( $@ ) {
    plan skip_all => "HTTP::Request or HTTP::Request::AsCGI or CGI is not installed: $@";
}

plan tests => 5 + 5;

require $script;

our @plugins = ();
our @first   = ();

{
    no warnings;
    package minish;
    
    *run_plugins = sub {
        my ( $app, $method, @args ) = @_;
        push @plugins, [ $method, @args ];
    };
    *run_plugin_first = sub {
        my ( $app, $method, @args ) = @_;
        push @first, [ $method, @args ];
        return;
    };
}

my $app = minish->new;
   $app->merge_config({
        pages => {
            dir => "${example}/core/run_run/pages"
        },
        flavour => {
            dir => "${example}/core/run_run/flavours",
        }
   });
   $app->setup_pages;
   $app->path_info('foo.html');
   $app->path_arguments([qw( foo )]);
   $app->flavour('html');

# found
{
    my $req = HTTP::Request->new( GET => 'http://localhost/foo.html' );
    my $ctx = HTTP::Request::AsCGI->new($req)->setup;
    my $cgi = CGI->new;

    my $app = minish->new;

    $app->cgi( $cgi );
    $app->plugins([]);
    $app->prepare;
    $app->run;

    is_deeply(
        [ @plugins ],
        [
            ['prepare'],
            ['head',    \q{head.html}],
            ['page', minish::page->new( dir => "${example}/core/run_run/pages", filename => 'foo', file_extension => 'txt'), \q{page.html} ],
            ['foot',    \q{foot.html}],
            ['finalize'],
        ],
    );
    is_deeply(
        [ @first ],
        [
            ['skip'],
        ],
    );

    is_deeply(
        $app->header,
        {
            -type   => 'text/html; charset=UTF-8',
            -status => '200 OK',
        }
    );
    is(
        $app->output,
        qq{head.htmlpage.htmlfoot.html},
    );

    ok( $ctx->response->as_string );
}

@plugins = ();
@first = ();

# not found
{
    my $req = HTTP::Request->new( GET => 'http://localhost/bar.html' );
    my $ctx = HTTP::Request::AsCGI->new($req)->setup;
    my $cgi = CGI->new;

    my $app = minish->new;

    $app->cgi( $cgi );
    $app->plugins([]);
    $app->prepare;
    $app->run;

    is_deeply(
        [ @plugins ],
        [
            ['prepare'],
            ['head',    \q{head.html}],
            ['page', minish::page->new( dir => "${example}/core/run_run/pages", filename => 'bar', file_extension => 'txt'), \q{page.html} ],
            ['foot',    \q{foot.html}],
            ['finalize'],
        ],
    );
    is_deeply(
        [ @first ],
        [
            ['skip'],
        ],
    );

    is_deeply(
        $app->header,
        {
            -type   => 'text/html; charset=UTF-8',
            -status => '200 OK',
        }
    );
    is(
        $app->output,
        qq{head.htmlpage.htmlfoot.html},
    );

    ok( $ctx->response->as_string );

}