#!perl

use strict;
use warnings;

use t::Util qw( $script );
use Test::More tests => 3;

require $script;

{
    no strict 'refs';
    no warnings 'redefine';
    package minish;
    for my $method ( qw( url path plugins ) ) {
        *{"minish::prepare_${method}"} = sub {
            package main;
            ok(1);
        };
    }
}

minish->prepare;