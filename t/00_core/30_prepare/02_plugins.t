#!perl

use strict;
use warnings;

package pluginA;

sub new { bless {}, shift }

sub setup { 1 }

sub start {
    my ( $self, $app ) = @_;
    $self->{'app'} = $app;
    return 0;
}

package pluginB;

use base qw( pluginA );

package main;

use t::Util qw( $script );
use Test::More tests => 4;

require $script;

my $pluginA = pluginA->new;
my $pluginB = pluginB->new;

minish->plugins([
    { plugin => $pluginA, on_off => 1 },
    { plugin => $pluginB, on_off => -1 },
]);

my $app = minish->new;
   $app->prepare_plugins;

is( $app->plugins->[0]->{'on_off'}, -1 );
is_deeply(
    $app->plugins->[0]->{'plugin'},
    { app => $app },
);

is( $app->plugins->[1]->{'on_off'}, -1 );
is_deeply(
    $app->plugins->[1]->{'plugin'},
    {},
);
