#!perl

use strict;
use warnings;

use t::Util qw( $script );
use Test::More;

require $script;

eval {
    use HTTP::Request;
    use HTTP::Request::AsCGI;
    use CGI;
};

if ( $@ ) {
    plan skip_all => "HTTP::Request or HTTP::Request::AsCGI or CGI is not installed: $@";
}

plan tests => 1;

{
    my $req = HTTP::Request->new( GET => 'http://localhost/foo/bar.html' );
    my $ctx = HTTP::Request::AsCGI->new($req)->setup;
    my $cgi = CGI->new;
    my $app = minish->new;

    $app->cgi( $cgi );
    $app->prepare_url;

    is( $app->url, 'http://localhost:80' );
}
