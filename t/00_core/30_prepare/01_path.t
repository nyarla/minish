#!perl

use strict;
use warnings;

use t::Util qw( $script );
use Test::More;

require $script;

eval {
    use HTTP::Request;
    use HTTP::Request::AsCGI;
    use CGI;
};

if ( $@ ) {
    plan skip_all => "HTTP::Request or HTTP::Request::AsCGI or CGI is not installed: $@";
}

plan tests => 3 + 3 + 2 + 2;

{
    my $req = HTTP::Request->new( GET => 'http://localhost/foo/bar.fox' );
    my $ctx = HTTP::Request::AsCGI->new($req)->setup;
    my $cgi = CGI->new;
    my $app = minish->new;

    $app->cgi( $cgi );
    $app->prepare_path;

    is( $app->flavour, 'fox' );
    is( $app->path_info, 'foo/bar.fox' );
    is_deeply(
        $app->path_arguments,
        [qw( foo bar )],
    );
}

{
    my $req = HTTP::Request->new( GET => 'http://localhost/foo/bar/baz' );
    my $ctx = HTTP::Request::AsCGI->new($req)->setup;
    my $cgi = CGI->new;
    my $app = minish->new;

    $app->merge_config({ flavour => { default => 'atom' } });

    $app->cgi( $cgi );
    $app->prepare_path;

    is( $app->flavour, 'atom' );
    is( $app->path_info, 'foo/bar/baz.atom' );
    is_deeply(
        $app->path_arguments,
        [qw( foo bar baz )],
    );
}

{
    my $req = HTTP::Request->new( GET => 'http://localhost/foo/bar' );
    my $ctx = HTTP::Request::AsCGI->new($req)->setup;
    my $cgi = CGI->new;
    my $app = minish->new;

    delete $app->config->{'flavour'}->{'default'};

    $app->cgi( $cgi );
    $app->prepare_path;

    is( $app->flavour, 'html' );
    is( $app->path_info, 'foo/bar.html' );
}

{
    my $req = HTTP::Request->new( GET => 'http://localhost/foo/bar/?flavour=cat' );
    my $ctx = HTTP::Request::AsCGI->new($req)->setup;
    my $cgi = CGI->new;
    my $app = minish->new;

    $app->cgi( $cgi );
    $app->prepare_path;

    is( $app->flavour, 'cat' );
    is( $app->path_info, 'foo/bar/index.cat' );
}
