package t::Util;

use strict;
use warnings;

use FindBin ();
use File::Spec;

use base qw( Exporter );
our @EXPORT_OK = qw( $script $basedir $example plugin );

our ( $script, $basedir, $example );
{
    my @path = File::Spec->splitdir( $FindBin::Bin );
    while ( my $dir = pop @path ) {
        if ( $dir eq 't' ) {
            $basedir = File::Spec->catfile( @path );
            $script  = File::Spec->catfile( @path, 'minish.pl' );
            $example = File::Spec->catfile( @path, 't', 'examples' );
            last;
        }
    }
}

sub plugin {
    my ( $name ) = @_;

    my $path    = "${basedir}/plugins/${name}";
    my $class   = "minish::plugin::${name}";

    return {
        path    => $path,
        class   => $class,
    }
}

$ENV{'MINISH_BOOTSTRAP'} = 0;

1;
